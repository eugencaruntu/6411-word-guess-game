# -*- coding: utf-8 -*-
"""
@Author: Eugen Caruntu
@ID:    29077103

This module implements the Game logic and score calculation.
It also provide a collection of games and means to output them to string
"""


class Game:
    """
    The Game representation
    Attributes:
        All the game properties
    """

    def __init__(self, game_id, word, menu, frequency):
        """
        Parameterized constructor
        :param game_id: The game ID
        :param word: The word to be played
        :param menu: The menu of the game
        :param frequency: The alphabet letter frequency
        """
        self.id = game_id
        self.m = menu
        self.f = frequency
        self.word = word
        self.status = 'Playing'
        self.selection = None

        self.guessed_so_far = '-' * len(word)
        self.bad_guesses = 0

        self.turned_letters = 0
        self.missed_letters = 0

        self.score = 0

    def play(self):
        """
        The game playing implementation, providing messages to user, checking and parsing the input.
        Game state and score is updated depending of matches from user input.
        The game will play until all letters in the word are found.
        :return: None
        """

        # loop until the word is fully matched
        while '-' in self.guessed_so_far:

            # print the guessed_so_far and the selection options
            print(self.m.current % self.guessed_so_far)
            self.selection = input(self.m.choices)

            # user wants to quit
            if self.selection == 'q':
                self.status = 'Gave up'
                print(self.m.quit)
                break

            # user wants to give-up
            elif self.selection == 't':
                self.status = 'Gave up'
                print(self.m.tell % self.word)
                break

            # user wants to guess
            elif self.selection == 'g':
                if input(self.m.guess) == self.word:
                    self.guessed_so_far = self.word
                else:
                    self.bad_guesses += 1
                    print(self.m.bad_guess)

            # user decides to enter a letter
            elif self.selection == 'l':
                letter = input(self.m.letter)
                self.turned_letters += 1

                # a correct letter
                if letter in self.word:
                    print(self.m.matched_letter % self.matching_letter(letter))

                # an incorrect letter
                else:
                    self.missed_letters += 1
                    print(self.m.missed_letter)

            # user made an incorrect selection
            else:
                print(self.m.bad_entry)

        # once out of the loop, if the word was fully guessed, declare success
        if self.guessed_so_far == self.word:
            self.status = 'Success'
            print(self.m.success % self.word)

        self.calculate_score()

    def matching_letter(self, letter):
        """
        Updates the guessed_so_far with the letter matched.
        The method make a list of the guessed_so_far, replace all matched positions, then converts, back to string)
        :param letter: The letter matched
        :return: A count of matches for the letter
        """
        count = 0
        tmp = list(self.guessed_so_far)
        for i, l in enumerate(self.word):
            if l is letter:
                count += 1
                tmp[i] = letter
        self.guessed_so_far = ''.join(tmp)
        return count

    def calculate_score(self):
        """
        Calculates the score depending on the situation:
        Success: First divide total value by the amount of letter turned (or one if no letter was attempted)
        Gave up: negative value of unturned letters
        In both of these cases we also remove the 10% penalty for each of the bad guess attempts
        :return: None
        """
        if self.status == 'Success':
            self.score = self.word_value() / (self.turned_letters if self.turned_letters != 0 else 1)
        elif self.status == 'Gave up':
            self.score = -self.unturned_value()
        self.score -= 0.1 * abs(self.score) * self.bad_guesses

    def unturned_value(self):
        """
        Sum-up the frequencies for each letter that is not yet uncovered
        :return: The sum of missing letters' frequencies
        """
        unturned = 0
        for i, l in enumerate(self.guessed_so_far):
            if l is '-':
                unturned += self.f[self.word[i]]
        return unturned

    def word_value(self):
        """
        Sum-up the frequencies for each letter in entire word
        :return: The sum of letters' frequencies
        """
        full = 0
        for l in self.word:
            full += self.f[l]
        return full

    def __str__(self):
        """
        Overrides __str__ for a pretty display of the game object
        :return: A formatted string of the game
        """
        return '{: <8} {: <8} {: <12} {: <15} {: <18} {: <8}'.format(str(self.id), self.word, self.status, str(self.bad_guesses), str(self.missed_letters), str(round(self.score, 2)))


class Tournament:
    """
    A collection of games
    Attributes:
        history (dict()): A dictionary of games
    """

    def __init__(self):
        """
        Default constructor for the tournament.
        """
        self.history = dict()

    def __str__(self):
        """
        Overrides __str__ for a pretty display of the tournament
        :return: A formatted string of the tournament's games
        """
        s = '\n'
        s += '{: <8} {: <8} {: <12} {: <15} {: <18} {: <8}\n'.format('Game', 'Word', 'Status', 'Bad Guesses', 'Missed Letters', 'Score')
        s += '{: <8} {: <8} {: <12} {: <15} {: <18} {: <8}\n'.format('----', '----', '------', '-----------', '--------------', '-----')
        final_score = 0
        for g in self.history.values():
            s += str(g)
            s += '\n'
            final_score += g.score
        s += '\nFinal Score: ' + str(round(final_score, 2))
        return s
