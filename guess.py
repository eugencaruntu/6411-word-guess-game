# -*- coding: utf-8 -*-
"""
@Author: Eugen Caruntu
@ID:    29077103

A Word Guessing Game implementation:
    - the application loads a text file into a trie representation in memory
    - it then randomly selects a word and creates games
    - each game is played and added to a tournament collection
    - the score is calculated after each game and displayed when user quits the application

This module is the driver of the application.
It also implements the Menu interface
"""
from stringDatabase import Trie, Frequency
from game import Game, Tournament


class Menu:
    """
    The representation for the menu options.
    Attributes:
        Each attribute is a string representation of the message to be displayed
    """

    def __init__(self):
        """
        Instantiate the menu messages
        """
        self.welcome = '\n*** Welcome to the Word Guessing Game! Have fun... trying! ***'
        self.current = '\n\nCurrent Guess: %s\n'
        self.choices = 'g = guess, t = tell me, l = letter, q = quit\n'
        self.guess = 'Enter the entire word you want to guess:\n'
        self.letter = 'Enter an alphabet letter:\n'
        self.tell = '\t[ Sadly you gave-up. The word was: %s ]'
        self.quit = '\t[ Sorry to see you quit. Goodbye! ]\n'
        self.bad_entry = '\t[ The selection was invalid. Please retry. ]'
        self.bad_guess = '\t[ The word you guessed was wrong! ]'
        self.matched_letter = '\t[ You found %d letters! ]'
        self.missed_letter = '\t[ The letter you entered was not matched! ]'
        self.success = '\t[ YOU WON! You found the word %s. Congratulations! ]'
        self.max_games = '\t[ You lost after 100 games played. Maybe next time you get more lucky... ]'


if __name__ == '__main__':
    """
    Parse the file and play the Games which are collected in a Tournament object
    """

    # parse a file and create a trie representation in memory
    corpus = Trie('four_letters.txt')

    # Make one object for each Tournament, Menu and Frequencies so they will be passed later into each played game
    games = Tournament()
    menu = Menu()
    frequency = Frequency().alphabet

    # Start and play games until user quits or 100 games were played
    print(menu.welcome)
    while True:

        # randomly select a word to play
        word = corpus.random_dfs('', corpus.root)

        # make a new game and add it to the tournament collection
        g = Game(len(games.history) + 1, word, menu, frequency)
        games.history[g.id] = g

        # play the game tournament until user decides to quit or reached 100 games played
        g.play()
        if g.selection == 'q':
            break
        if len(games.history.values()) == 100:
            print(g.m.max_games)
            break

    # display the results
    print(games)
