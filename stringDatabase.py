# -*- coding: utf-8 -*-
"""
@Author: Eugen Caruntu
@ID:    29077103

This module is responsible for reading and parsing the file and representing the corpus in memory as a trie object.
It implements the Trie and Node as well as the alphabet letter's Frequencies
"""
import random


class Trie:
    """
    The corpus is represented as a Trie in order to minimize the memory footprint.
    Methods for parsing a file, inserting words and navigating the trie are implemented (including a random traversal)
    Attributes:
        root (Node): the root node of the trie
    """

    def __init__(self, file_name):
        """
        Parameterized constructor parsing a file and inserting its words into the trie
        :param file_name: The file name to be parsed
        """
        self.root = Node('|')
        self.parse_file(file_name)

    def parse_file(self, file_name):
        """
        Parse a file and insert each word in the trie object
        :param file_name: The file name to be parsed
        :return: None
        """
        with open(file_name, 'r') as file:
            for line in file:
                for word in line.split():
                    self.insert(word)

    def insert(self, word):
        """
        Insert a word in trie, one character at a time and mark the last character as end of word.
        Children nodes are created if not found already.
        :param word: The word to be inserted
        :return: None
        """
        current_node = self.root
        for c in word:
            if c not in current_node.children:
                current_node.children[c] = Node(c)
            current_node = current_node.children[c]
        current_node.word_end = True  # mark the last letter as word end

    def dfs(self, word, current_node):
        """
        A DFS traversal for entire trie
        :param word: The word being read while traversing the trie
        :param current_node: The current node being visited
        :return: The word that was read once a leaf is reached
        """
        if current_node.word_end:
            return word
        else:
            for k, v in current_node.children.items():
                return self.dfs(word + k, v)

    def random_dfs(self, word, current_node):
        """
        A similar DFS traversal that is randomly visiting only one child node and returns a single word from entire trie
        :param word: The word being read while traversing the trie
        :param current_node: The current node being visited
        :return: The single word that was read once a leaf is reached
        """
        if current_node.word_end:
            return word
        else:
            (k, v) = random.choice(list(current_node.children.items()))
            return self.random_dfs(word + k, v)


class Node:
    """
    The trie node representation.
    Attributes:
         character (char): The character represented by the trie node
         children (dict(char, Node)): All the node children encountered in vocabulary
         word_end (boolean): The flag representing the end of a word (will be set to true for the leafs)
    """

    def __init__(self, character):
        """
        Parameterized constructor to instantiate a trie node
        :param character: The character to be used to create the node
        """
        self.character = character
        self.children = dict()
        self.word_end = False


class Frequency:
    """
    The frequency of each letter in English alphabet to be used for score calculation
    Attributes:
            alphabet (dict(char, double)): The frequency dictionary
    """

    def __init__(self):
        """
        The frequency is being captured in a dictionary having each letter as a key
        """
        self.alphabet = {'a': 8.17, 'b': 1.49, 'c': 2.78, 'd': 4.25, 'e': 12.70, 'f': 2.23, 'g': 2.02, 'h': 6.09, 'i': 6.97, 'j': 0.15, 'k': 0.77, 'l': 4.03, 'm': 2.41, 'n': 6.75, 'o': 7.51, 'p': 1.93, 'q': 0.10, 'r': 5.99, 's': 6.33, 't': 9.06, 'u': 2.76, 'v': 0.98, 'w': 2.36, 'x': 0.15, 'y': 1.97, 'z': 0.07}
